---
title: "Monado - SteamVR Plugin"
layout: main
---

Monado 21.0.0/master comes with a SteamVR plugin.

It enables the use of Monado's VR hardware drivers with Valve's SteamVR runtime.

It does **not** make it possible to run OpenVR games and applications on Monado/OpenXR runtimes. See [OpenComposite](https://gitlab.com/znixian/OpenOVR) for an in-development project focusing on this use case.

# Current status

* All HMDs supported by Monado
  * Distortion correction for OpenHMD, Panotools (PSVR), Vive/Vive Pro/Valve Index
  * Arbitrarily orientated displays from `get_view_pose()`, e.g. canted displays like Valve Index
* All controllers supported by Monado
  * Since Monado v21.0.0: All controllers supported by Monado use generated SteamVR input profiles.

# Registering the plugin

The SteamVR plugin is built by default and is installed in `$PREFIX/share/steamvr-monado`. SteamVR will use the plugin only after it is registered with SteamVR's `vrpathreg` tool.

If Monado was installed in the /usr prefix, the command is
```
~/.steam/steam/steamapps/common/SteamVR/bin/vrpathreg.sh adddriver /usr/share/steamvr-monado
```

The SteamVR plugin will also be placed in the Monado build tree where it can be registered with SteamVR without installing Monado.
```
~/.steam/steam/steamapps/common/SteamVR/bin/vrpathreg.sh adddriver ~/monado/build/steamvr-monado
```

Depending on the distribution or Steam installation method, Steam may be installed in `~/.local/share/Steam` rather than `~/.steam/steam`.
```
~/.local/share/Steam/steamapps/common/SteamVR/bin/vrpathreg.sh adddriver /usr/share/steamvr-monado
```

Running `vrpathreg` without arguments lists currently registered plugins listed in "External Drivers".
```
~/.steam/steam/steamapps/common/SteamVR/bin/vrpathreg.sh

<...>
External Drivers:
        /usr/share/steamvr-monado
```

Plugins can be unregistered from Steam by using `vrpathreg.sh removedriver`.
```
~/.steam/steam/steamapps/common/SteamVR/bin/vrpathreg.sh removedriver /usr/share/steamvr-monado
```

After starting SteamVR, debug output for the plugin can be found in `~/.steam/steam/logs/vrserver.txt`. It will be prefixed with `monado:`.

# Input Profiles

Since v21.0.0 Monado automatically generates input profiles for SteamVR.

The source database for this generator is [bindings.json](https://gitlab.freedesktop.org/monado/monado/-/blob/v21.0.0/src/xrt/auxiliary/bindings/bindings.json) and the generated profiles are placed in build/steamvr-monado/resources/input. Note that not all of the generated input profiles actually have an associated driver in Monado just yet.

Even though no default bindings are generated at this time quite a lot of the SteamVR functionality works out of the box. Unfortunately not everything works well, especially games using the legacy Steam input APIs.

There are various tutorials how to create legacy bindings for games arund, for example [here](https://steamcommunity.com/sharedfiles/filedetails/?id=1545634111). Some games or applications also require the `Pretend to be this type of controller` option set to `Vive Controller`.

As an alternative, Monado-SteamVR offers an option to use SteamVR's own Index Controller input profile and emulate an Index controller with any of Monado's controllers. Note that with this profile games may expect inputs that are not possible to produce on other controllers.

To enable Index Controller emulation, use `STEAMVR_EMULATE_INDEX_CONTROLLER=1`. SteamVR can be started from a command line:

```
STEAMVR_EMULATE_INDEX_CONTROLLER=1 ~/.steam/steam/steamapps/common/SteamVR/bin/vrstartup.sh
```

Or in the Steam Library in the SteamVR properties the variable can be added to the startup options:

```
STEAMVR_EMULATE_INDEX_CONTROLLER=1 %command%
```
